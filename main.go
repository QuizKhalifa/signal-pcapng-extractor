package main

import (
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
	"github.com/google/gopacket/pcapgo"
)

var (
	InetAddr string
	SrcIP    string
	DstIP    string
)

type DnsMsg struct {
	Timestamp       string
	SourceIP        string
	DestinationIP   string
	DnsQuery        string
	DnsAnswer       []string
	DnsAnswerTTL    []string
	NumberOfAnswers string
	DnsResponseCode string
	DnsOpCode       string
}

// Domains
var (
	DomainWhispersystem string = ".whispersystems.org"
	DomainSignal        string = ".signal.org"
)

func main() {
	var ips []string
	if handle, err := pcap.OpenOffline("./signal.pcapng"); err != nil {
		panic(err)
	} else {
		ips = dns(handle)
		handle.Close()
	}

	// Reopen file to remove some bugs
	if handle, err := pcap.OpenOffline("./signal.pcapng"); err != nil {
		panic(err)
	} else {
		defer handle.Close()

		// From "Detecting a valid IPv4 in Go like a boss"
		// https://medium.com/@sergio.anguita/detecting-a-valid-ipv4-in-go-like-a-boss-32eda9bf422f
		// The "regex method"
		ipRe, _ := regexp.Compile(`^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$`)

		// combine the ips we found into a valid BPF filter that can find any packet to or from the IPs
		for i, ip := range ips {
			if ipRe.MatchString(ip) {
				ips[i] = "ip host " + ip
			} else {
				ips[i] = "ip6 host " + ip
			}
		}
		filter := strings.Join(ips, " or ")

		// Set the filter
		handle.SetBPFFilter(filter)

		// This is the file we'll write to
		f, err := os.Create("out.pcapng")
		if err != nil {
			panic(err)
		}
		defer f.Close()

		// Writer that will write the pcap data
		r, err := pcapgo.NewNgWriter(f, handle.LinkType())
		if err != nil {
			panic(err)
		}
		defer r.Flush()

		// Write the packets that are matched by the BPF filter
		pkgsrc := gopacket.NewPacketSource(handle, handle.LinkType())
		for packet := range pkgsrc.Packets() {
			//fmt.Println(packet.String())
			if err := r.WritePacket(packet.Metadata().CaptureInfo, packet.Data()); err != nil {
				log.Fatalf("pcap.WritePacket(): %v", err)
			}
		}
	}
}

// Stolen and modified from https://gist.github.com/dvas0004/3d280f95a83a3084b314ea208f19f9e9
func dns(handle *pcap.Handle) []string {
	var ips []string = []string{}

	var eth layers.Ethernet
	var ip4 layers.IPv4
	var ip6 layers.IPv6
	var tcp layers.TCP
	var udp layers.UDP
	var dns layers.DNS

	var payload gopacket.Payload

	// Get IP for all domains in use
	handle.SetBPFFilter("dns and dst host 192.168.10.112")

	parser := gopacket.NewDecodingLayerParser(layers.LayerTypeEthernet, &eth, &ip4, &ip6, &tcp, &udp, &dns, &payload)

	decodedLayers := make([]gopacket.LayerType, 0, 10)
	for {
		data, _, err := handle.ReadPacketData()
		if err != nil {
			return unique(ips)
		}

		_ = parser.DecodeLayers(data, &decodedLayers)
		for _, typ := range decodedLayers {
			switch typ {
			case layers.LayerTypeIPv4:
				SrcIP = ip4.SrcIP.String()
				DstIP = ip4.DstIP.String()
			case layers.LayerTypeIPv6:
				SrcIP = ip6.SrcIP.String()
				DstIP = ip6.DstIP.String()
			case layers.LayerTypeDNS:
				dnsOpCode := int(dns.OpCode)
				dnsResponseCode := int(dns.ResponseCode)
				dnsANCount := int(dns.ANCount)

				if (dnsANCount == 0 && dnsResponseCode > 0) || (dnsANCount > 0) {

					for _, dnsQuestion := range dns.Questions {

						t := time.Now()
						timestamp := t.Format(time.RFC3339)

						// Add a document to the index
						d := DnsMsg{Timestamp: timestamp, SourceIP: SrcIP,
							DestinationIP:   DstIP,
							DnsQuery:        string(dnsQuestion.Name),
							DnsOpCode:       strconv.Itoa(dnsOpCode),
							DnsResponseCode: strconv.Itoa(dnsResponseCode),
							NumberOfAnswers: strconv.Itoa(dnsANCount)}

						if dnsANCount > 0 {
							if strings.Contains(string(dnsQuestion.Name), DomainWhispersystem) || strings.Contains(string(dnsQuestion.Name), DomainSignal) {
								for _, dnsAnswer := range dns.Answers {
									d.DnsAnswerTTL = append(d.DnsAnswerTTL, fmt.Sprint(dnsAnswer.TTL))
									if dnsAnswer.IP.String() != "<nil>" {
										//fmt.Println("<"+string(dnsQuestion.Name)+">: ", dnsAnswer.IP.String())
										ips = append(ips, dnsAnswer.IP.String())
										d.DnsAnswer = append(d.DnsAnswer, dnsAnswer.IP.String())
									}
								}
							}

						}
					}
				}

			}
		}
	}
}

func unique(strSlice []string) []string {
	keys := make(map[string]bool)
	list := []string{}
	for _, entry := range strSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}
