# Signal pcapng Extractor

The go script will try to extract all Signal-messenger-related packets (except for UDP) and save it into a new pcapng file.

By default it takes a file called `signal.pcapng` and puts out a `out.pcapng` file.

## Usage

Put the pcapng file in the same folder as the script and run it with 

```BASH
go run main.go
```